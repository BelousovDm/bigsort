package sort;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 * Класс для сортировки больших текстовых файлов, которые не влезают в оперативную память.
 * Используется сортировка методом вставки, где исходный файл разделяется на множество малых отсортированных файлов.
 * Далее происходит чтение из каждого малого файла по одной строчке в лист строк, потом находится наименьшая из листа и
 * вставляется в файл записи. После читается следующая строка из файла вставленной строки, и заменяется на место записанной строки.
 * После окончания записи поток закрывается и удаляются промежуточные файлы.
 *
 * @author  Dmitry Belousov
 */
public class BigFileSort {
    /** Дефолтная переменная означающая количество строк хранимое в памяти. */
    private static final int DEFAULT_STRING_COUNT_PER_FILE = 1000000;

    /** Дефолтное название отсортированного файла */
    private static final String DEFAULT_OUTPUT_FILE = "SortedList";

    /** Название файла для чтения. */
    private String inputFileName;

    /** Название файла для записи. */
    private String outputFileName;

    /** Количество строк хранимое в памяти. */
    private int stringCountPerFile;

    /** Количество файлов на которое разбивается исходный файл*/
    private int countSplitFiles;

    /** Конструктор для создания объекта,
     * с заданным файлом записи и чтения
     *
     * @param inputFileName название файла чтения
     * @param outputFileName название файла записи
     *
     * */
    BigFileSort(String inputFileName, String outputFileName) {
        this.inputFileName = inputFileName;
        this.outputFileName = outputFileName;
        this.stringCountPerFile = DEFAULT_STRING_COUNT_PER_FILE;
    }

    /** Конструктор для создания объекта.
     * Название файла записи используется дефолтное.
     *
     * @param inputFileName название файла чтения
     *
     * */
    BigFileSort(String inputFileName) {
        this.inputFileName = inputFileName;
        this.outputFileName = DEFAULT_OUTPUT_FILE;
        this.stringCountPerFile = DEFAULT_STRING_COUNT_PER_FILE;
    }

/** Сортируется файл, указанный в объекте.
 *  Создается лист потоков чтения в количестве промежуточных файлов.
 *  Читается по строке из промежуточных файлов в лист. Далее пока существуют
 *  потоки чтения находим минимальную строку и записываем в выходной файл.
 *  Читаем из этого файла следующую строку и заменяем её на вставленную в листе строк.
 *  В случае окончания промежуточного файла, поток чтения удаляется из листа чтения потоков.
 *  Так происходит до тех пор, пока есть непрочитанные строки с промежуточных файлов.
 *  При окончании сортировки, все промежуточные файлы удаляются.
 *
 * @throws IOException в случае ошибки с потоками */
    public void sortFile() throws IOException {
        splitFiles();
        ArrayList<BufferedReader> listReader = new ArrayList<>();
        FileWriter writer = new FileWriter(outputFileName);
        for (int i = 1; i <= countSplitFiles; i++) {
            listReader.add(new BufferedReader(new FileReader(Integer.toString(i))));
        }
        String line;
        ArrayList<String> strList = new ArrayList<>();
        for (BufferedReader read : listReader) {
            line = read.readLine();
            strList.add(line);
        }
        while (!listReader.isEmpty()) {
            String strToOut = Collections.min(strList);
            int minIndex = strList.indexOf(strToOut);
            writer.write(strToOut + "\n");
            line = listReader.get(minIndex).readLine();

            if (line == null) {
                listReader.get(minIndex).close();
                listReader.remove(minIndex);
                strList.remove(minIndex);
            } else {
                strList.set(minIndex, line);
            }
        }
        writer.close();
        deleteUsedFiles(countSplitFiles);
    }

    /** Удаление промежуточных файлов использованных для сортировки
     */
    private void deleteUsedFiles(int fileCounts) {
        for (int i = 1; i <= fileCounts; i++) {
            new File(Integer.toString(i)).delete();
        }
    }

    /** Происходит чтение необходимого количества строк из большого файла,
     *  прочтенные строки сортируются и записываются в промежуточный файл.
     *  Так происходит до окончания чтения файла.
     *  Записывается количество разделенных файлов.
     *
     * @throws FileNotFoundException если файл для чтения не существует
     * @throws IOException в случае ошибки с потоками
     */
    private void splitFiles() throws IOException {
        File file = new File(inputFileName);
        if(!file.exists())
            throw new FileNotFoundException("File does not exist");
        FileReader reader = new FileReader(file);
        BufferedReader buffReader = new BufferedReader(reader);
        List<String> str = new ArrayList<>();
        String line;
        countSplitFiles = 0;
        boolean flag = true;
        while (flag) {
            countSplitFiles++;
            str.clear();
            for (int i = 0; i < stringCountPerFile; i++) {
                line = buffReader.readLine();
                if (line == null) {
                    flag = false;
                    if (i == 0) {
                        --countSplitFiles;
                        return;
                    }
                    break;
                }
                str.add(line);
            }
            Collections.sort(str);
            FileWriter writer = new FileWriter(Integer.toString(countSplitFiles));
            for (String s : str) {
                writer.write(s + "\n");
            }
            writer.close();
        }
        buffReader.close();
    }

    /** Получение строки файла чтения
     *
     * @return название строки файла чтения
     */
    public String getInputFileName() {
        return inputFileName;
    }

    /** Установка названия файла чтения
     *
     * @param inputFileName название файла чтения
     */
    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }
    /** Получение строки файла чтения
     *
     * @return название строки файла чтения
     */
    public String getOutputFileName() {
        return outputFileName;
    }
    /** Установка названия файла для записи
     *
     * @param outputFileName названия файла для записи
     */
    public void setOutputFileName(String outputFileName) {
        this.outputFileName = outputFileName;
    }

    /** Получение количества строк для чтения и сортировки из большого файла
     *
     * @return количество строк для чтения
     */
    public int getStringCountPerFile() {
        return stringCountPerFile;
    }

    /** Установка количества строк для чтения и сортировки из большого файла
     *
     * @param  stringCountPerFile количество строк для чтения
     */
    public void setStringCountPerFile(int stringCountPerFile) {
        this.stringCountPerFile = stringCountPerFile;
    }
}
